/*
 * Copyright (C) 2016 Christoffer Andersson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chriand.remotegpio.network;

import chriand.network.ConnectionServer;
import chriand.network.IPacketListener;
import chriand.network.IServer;
import chriand.network.IServerAccess;
import chriand.network.NetworkData;
import chriand.network.Server;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Christoffer Andersson
 */
public class ServerHandler implements IServer
{
    private Server server;
    
    public ServerHandler() throws IOException
    {
        this.server = new Server(this);
        this.server.bind(InetAddress.getLocalHost().getHostAddress(), 4444, 10);
    }
    
    public void shutdown()
    {
        this.server.shutdown();
    }
    
    public void update()
    {
        this.server.update();
    }

    @Override
    public ConnectionServer createConnection(Socket socket, IServerAccess server, NetworkData networkData)
    {
        try
        {
            return new ConnectionRPIServer(socket, server);
        }
        catch (SocketException ex)
        {
            Logger.getLogger(ServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void sendPacket(NetworkData networkData, IPacketListener listener)
    {
        synchronized(server.getServer().getConnections())
        {
            for(ConnectionServer connectionServer : server.getServer().getConnections())
            {
                if(connectionServer != null)
                {
                    connectionServer.sendPacketTCP(networkData, listener);
                }
            }
        }
    }

    @Override
    public void onConnectionLost(ConnectionServer connectionServer)
    {
        
    }
}