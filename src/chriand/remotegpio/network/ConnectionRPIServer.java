/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.remotegpio.network;

import chriand.remotegpio.core.RemoteGPIO;
import chriand.core.serialization.SerializableTypeNotFoundException;
import chriand.network.ConnectionServer;
import chriand.network.IServerAccess;
import chriand.network.NetworkData;
import chriand.network.PacketXML;
import com.pi4j.io.gpio.RaspiPin;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public class ConnectionRPIServer extends ConnectionServer
{
    public ConnectionRPIServer(Socket socket, IServerAccess server) throws SocketException
    {
        super(socket, server);
    }

    @Override
    protected void onConnectionLost()
    {
        super.onConnectionLost();
        RemoteGPIO.geNetworkListener().onConnectionLost();
    }

    @Override
    public void onConnectionEstablished()
    {
        super.onConnectionEstablished();
        RemoteGPIO.geNetworkListener().onConnectionEstablished();
    }

    @Override
    protected boolean onPacketRequestReceived(NetworkData networkData)
    {
        if(networkData.getPacketType() == PacketType.GPIOEvent)
        {
            try
            {
                PacketXML packetXML = new PacketXML(networkData.getData()[0]);
                RemoteGPIO.geNetworkListener().onGPIOEventReceived(RaspiPin.getPinByName(packetXML.getXML().getString("Name")), packetXML.getXML().getBoolean("State"));
            }
            catch (SerializableTypeNotFoundException ex)
            {
                Logger.getLogger(ConnectionRPIServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            RemoteGPIO.geNetworkListener().onPacketReceived(networkData.createPacketDivider(), networkData.getPacketType());
        }
        return super.onPacketRequestReceived(networkData);
    }
}