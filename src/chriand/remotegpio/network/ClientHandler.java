/*
 * Copyright (C) 2016 Christoffer Andersson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chriand.remotegpio.network;

import chriand.network.ClientManager;
import chriand.network.ConnectionClient;
import chriand.network.IClientManager;
import chriand.network.IPacketListener;
import chriand.network.NetworkData;
import chriand.network.PacketXML;
import chriand.remotegpio.core.RemoteGPIO;
import chriand.core.serialization.SerializableTypeNotFoundException;
import com.pi4j.io.gpio.RaspiPin;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public class ClientHandler implements IClientManager
{
    @Override
    public void onConnectionLost()
    {
        RemoteGPIO.geNetworkListener().onConnectionLost();
        if(RemoteGPIO.getAutoReconnect())
        {
            ClientManager.connect(RemoteGPIO.getAddress(), 4444, this);
        }
    }

    @Override
    public void onConnectionEstablished()
    {
        RemoteGPIO.geNetworkListener().onConnectionEstablished();
    }

    @Override
    public ConnectionClient createConnection(String address, int port)
    {
        try
        {
            return new ConnectionClient(address, port, new NetworkData());
        }
        catch (SocketException ex)
        {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean onPacketRequestReceived(NetworkData networkData)
    {
        if(networkData.getPacketType() == PacketType.GPIOEvent)
        {
            try
            {
                PacketXML packetXML = new PacketXML(networkData.getBytes());
                RemoteGPIO.geNetworkListener().onGPIOEventReceived(RaspiPin.getPinByName(packetXML.getXML().getString("Name")), packetXML.getXML().getBoolean("State"));
            }
            catch (SerializableTypeNotFoundException ex)
            {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            RemoteGPIO.geNetworkListener().onPacketReceived(networkData.createPacketDivider(), networkData.getPacketType());
        }
        return false;
    }
    
    public void sendPacket(NetworkData networkData, IPacketListener iPacketListener)
    {
        ClientManager.sendPacketTCP(networkData, iPacketListener);
    }
    
    public void update()
    {
        ClientManager.update();
    }
    
    public void shutdown()
    {
        ClientManager.shutdown();
    }
}
