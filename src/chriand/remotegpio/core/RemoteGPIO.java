/*
 * Copyright (C) 2016 Christoffer Andersson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chriand.remotegpio.core;

import chriand.remotegpio.network.ClientHandler;
import chriand.network.ClientManager;
import chriand.remotegpio.network.INetworkListener;
import chriand.network.IPacketListener;
import chriand.network.NetworkData;
import chriand.network.PacketDivider;
import chriand.remotegpio.network.PacketType;
import chriand.network.PacketXML;
import chriand.network.lan.IServerLANListener;
import chriand.network.lan.LocalAreaNetwork;
import chriand.remotegpio.network.ServerHandler;
import com.pi4j.io.gpio.Pin;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public final class RemoteGPIO implements IServerLANListener
{
    private static RemoteGPIO instance;
    private static ClientHandler clientHandler;
    private static ServerHandler serverHandler;
    private static String identification;
    private static String address;
    private static boolean shutdown;
    private static boolean autoReconnect;
    private static INetworkListener networkListener;
    
    private RemoteGPIO()
    {
        
    }
    
    /**
     * Used to initiate the server and may only be called once on the Raspberry Pi side
     * 
     * @param identification    A unique id to help Raspberry Pi and the Remote controller identify each other
     * @param listener          Receives events about the network activity
     */
    public static void initAsRaspberryPi(String identification, INetworkListener listener)
    {
        if(RemoteGPIO.networkListener == null)
        {
            try
            {
                RemoteGPIO.networkListener = listener;
                RemoteGPIO.serverHandler = new ServerHandler();
                LocalAreaNetwork.bindServer(identification, 4444, 1000);
            }
            catch (IOException ex)
            {
                Logger.getLogger(RemoteGPIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * Used to initiate the client and may only be called once on the Remote controller side
     * 
     * @param identification    A unique id to help Raspberry Pi and the Remote controller identify each other
     * @param autoReconnect     Lets the client auto reconnect if the connections is lost
     * @param listener          Receives events about the network activity
     */
    public static void initAsRemote(String identification, boolean autoReconnect, INetworkListener listener)
    {
        if(RemoteGPIO.networkListener == null)
        {
            RemoteGPIO.networkListener = listener;
            RemoteGPIO.autoReconnect = autoReconnect;
            RemoteGPIO.instance = new RemoteGPIO();
            LocalAreaNetwork.bindClient(instance, identification, 4444, 1000);
        }
    }
    
    /**
     * Used to shutdown the RemoteGPIO library
     */
    public static void shutdown()
    {
        RemoteGPIO.shutdown = true;
        RemoteGPIO.networkListener = null;
        if(LocalAreaNetwork.getInstance() != null)
        {
            LocalAreaNetwork.getInstance().shutdown();
        }
        if(clientHandler != null)
        {
            RemoteGPIO.clientHandler.shutdown();
        }
        else if(serverHandler != null)
        {
            RemoteGPIO.serverHandler.shutdown();
        }
        RemoteGPIO.instance = null;
    }
    
    /**
     * @return  True if the auto reconnect is active
     */
    public static boolean getAutoReconnect()
    {
        return autoReconnect;
    }
    
    /**
     * @return  The IP address of the other device
     */
    public static String getAddress()
    {
        return address;
    }
    
    /**
     * @return  The network connectivity listener
     */
    public static INetworkListener geNetworkListener()
    {
        return networkListener;
    }
    
    /**
     * Used to send a GPIO event to the other device
     * 
     * @param pin       The pin to be affected
     * @param state     The new state of the pin such as ON or OFF
     */
    public static void sendGPIOEvent(Pin pin, boolean state)
    {
        RemoteGPIO.sendGPIOEvent(pin, state, null);
    }
    
    /**
     * Used to send a GPIO event to the other device
     * 
     * @param pin       The pin to be affected
     * @param state     The new state of the pin such as ON or OFF
     * @param listener  A callback listener used to get a response of the packet delivery
     */
    public static void sendGPIOEvent(Pin pin, boolean state, IPacketListener listener)
    {
        RemoteGPIO.sendPacket(new NetworkData(new PacketXML(PacketXML.createRootNode().add(pin.getName(), "Name").add(state, "State")), PacketType.GPIOEvent), listener);
    }
    
    /**
     * Used to send a custom packet to the other device
     * 
     * @param divider       A container of packets to send
     * @param packetType    The type of the packet
     * @param listener      A callback listener used to get a response of the packet delivery
     */
    public static void sendPacket(PacketDivider divider, int packetType, IPacketListener listener)
    {
        RemoteGPIO.sendPacket(new NetworkData(divider, packetType), listener);
    }
    
    /**
     * Used to send a custom packet to the other device
     * 
     * @param networkData   A complete container of information to be sent
     * @param listener      A callback listener used to get a response of the packet delivery
     */
    public static void sendPacket(NetworkData networkData, IPacketListener listener)
    {
        if(clientHandler != null)
        {
            clientHandler.sendPacket(networkData, listener);
        }
        else if(serverHandler != null)
        {
            serverHandler.sendPacket(networkData, listener);
        }
    }

    @Override
    public boolean onServerFound(String address, String userdata)
    {
        RemoteGPIO.address = address;
        RemoteGPIO.connect(address);
        return false;
    }

    @Override
    public void onServerNotFound()
    {
        if(!shutdown)
        {
            LocalAreaNetwork.bindClient(instance, identification, 4444, 1000);
        }
    }
    
    /**
     * Used to update the packet management
     * Usually called very frequently since it affects the network tick speed
     */
    public static void update()
    {
        if(clientHandler != null)
        {
            RemoteGPIO.clientHandler.update();
        }
        else if(serverHandler != null)
        {
            RemoteGPIO.serverHandler.update();
        }
    }
    
    private static void connect(String adress)
    {
        if(clientHandler != null)
        {
            RemoteGPIO.clientHandler.shutdown();
        }    
        RemoteGPIO.clientHandler = new ClientHandler();
        ClientManager.connect(adress, 4444, clientHandler);
    }
}