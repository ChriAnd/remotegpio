/*
 * Copyright (C) 2016 Christoffer Andersson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chriand.remotegpio.network;

import chriand.network.PacketDivider;
import com.pi4j.io.gpio.Pin;

/**
 *
 * @author Christoffer Andersson
 */
public interface INetworkListener
{
    /**
     * Called when a GPIO event is received from the other device such as
     * Raspberry Pi --> Remote controller
     *                  Or
     * Remote controller --> Raspberry Pi
     * 
     * @param pin       The pin that was affected
     * @param state     The new state of the pin such as ON or OFF
     */
    void onGPIOEventReceived(Pin pin, boolean state);
    
    /**
     * Called when a packet is received from the other device, that is NOT a GPIO event
     * This method can be used to receive custom packets
     * 
     * @param divider       A container of packets received
     * @param packetType    The type of the packet
     */
    void onPacketReceived(PacketDivider divider, int packetType);
    
    /**
     * Called when a connection is successfully made
     */
    void onConnectionEstablished();
    
    /**
     * Called when the connection is lost
     */
    void onConnectionLost();
}